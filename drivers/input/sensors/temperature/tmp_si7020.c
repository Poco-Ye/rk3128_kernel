/* drivers/input/sensors/temperature/tmp_si7020.c
 *
 * Copyright (C) 2012-2015 ROCKCHIP.
 * Author: luowei <lw@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/freezer.h>
#include <linux/of_gpio.h>
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif
#include <linux/sensor-dev.h>
#include <linux/proc_fs.h> 
#include <linux/uaccess.h>

static struct i2c_client *g_i2c_client = NULL;
static struct proc_dir_entry *proc_dir, *proc_humi, *proc_temp;  

static int get_humidity(struct i2c_client *client)  
{  
    unsigned int ret = 0;  
    int humi = 0;
    char tmp[2] = {0};  
    //printk(KERN_INFO "enter get_humi_val\n");  
  
    i2c_smbus_write_byte(client, 0xf5);  
    msleep(100);  
    i2c_master_recv(client, tmp, 2);  
    ret =((tmp[0]<<8)|tmp[1]);  
    if(ret < 0)  
        dev_err(&client->dev, "Read Error\n");  
      
    //printk(KERN_INFO "humi ret %d\n",ret);  
    humi = 125 * ret / 65536 - 6;  
    if(humi < 0)  
        humi = 0;  
    if(humi > 100)  
        humi = 100;  
    
    //printk("si7020 humidity : %d\n", humi);  
    return humi;
}  
  
static int get_temperature(struct i2c_client *client)  
{  
    int ret = 0; 
    int temp = 0; 
    char tmp[2] = {0};  
    //printk(KERN_INFO "enter get_temperature\n");  
  
    i2c_smbus_write_byte(client, 0xf3);  
    msleep(100);  
    i2c_master_recv(client, tmp, 2);  
    ret =((tmp[0]<<8)|tmp[1]);  
    if(ret < 0)  
        dev_err(&client->dev, "Read Error\n");  
      
    //printk(KERN_INFO "temperature ret %d\n",ret);  
    temp = 17572 * ret / 65536 - 4685;
    //printk("si7020 temperature : %d\n", temp);  
    return temp;
    //si7020->buf[temperature] = ret;  
    //printk("si7020->buf[temperature] : %d\n", si7020->buf[temperature]);  
    //printk(KERN_INFO "exit get_temperature\n");  
}  


static int temperature_report_value(struct input_dev *input, int data)
{
	//get temperature, high and temperature from register data

	input_report_abs(input, ABS_THROTTLE, data);
	input_sync(input);
	
	return 0;
}


static int sensor_report_value(struct i2c_client *client)
{
	struct sensor_private_data *sensor =
	    (struct sensor_private_data *) i2c_get_clientdata(client);	
    
	int result = 0;
	if(sensor->ops->read_len < 3)	//sensor->ops->read_len = 3
	{
		printk("%s:lenth is error,len=%d\n",__func__,sensor->ops->read_len);
		return -1;
	}

  result = get_temperature(client);
	temperature_report_value(sensor->input_dev, result);
		

	return result;
}


static int sensor_active(struct i2c_client *client, int enable, int rate)
{
	printk("tmp_si7020 sensor_active\n");	
	return 0;
}


static int sensor_init(struct i2c_client *client)
{
	printk("tmp_si7020 sensor_init\n");	
 g_i2c_client = client;
        	
	return 0;
}


struct sensor_operate temperature_si7020_ops = {
	.name				= "tmp_si7020",
	.type				= SENSOR_TYPE_TEMPERATURE,	//sensor type and it should be correct
	.id_i2c				= TEMPERATURE_ID_SI7020,	//i2c id number
	.read_reg			= SENSOR_UNKNOW_DATA,	//read data
	.read_len			= 3,			//data length
	.id_reg				= SENSOR_UNKNOW_DATA,	//read device id from this register
	.id_data 			= SENSOR_UNKNOW_DATA,	//device id
	.precision			= 24,			//8 bits
	.ctrl_reg 			= SENSOR_UNKNOW_DATA,	//enable or disable 
	.int_status_reg 		= SENSOR_UNKNOW_DATA,	//intterupt status register
	.range				= {100,65535},		//range
	.trig				= IRQF_TRIGGER_LOW | IRQF_ONESHOT | IRQF_SHARED,		
	.active				= sensor_active,	
	.init				= sensor_init,
	.report				= sensor_report_value,
};

static int proc_show_temp(struct seq_file *m, void *v) {
	 int temp = get_temperature(g_i2c_client);
	 seq_printf(m, "%d.%02d\n", temp/100, temp % 100);
	 return 0;
}
static int proc_open_temp(struct inode *inode, struct file *file) {
 return single_open(file, proc_show_temp, NULL);
}

static const struct file_operations proc_temp_ops = {
 .owner = THIS_MODULE,
 .write = NULL, 
 .open = proc_open_temp, 
 .read = seq_read,
 .llseek = NULL,
 .release = single_release,
};


static int proc_show_humi(struct seq_file *m, void *v) {

	 int humi = get_humidity(g_i2c_client);
	 seq_printf(m, "%d\n", humi);
	 return 0;
}

static int proc_open_humi(struct inode *inode, struct file *file) {
 return single_open(file, proc_show_humi, NULL);
}

static const struct file_operations proc_humi_ops = {
 .owner = THIS_MODULE,
 .write = NULL, 
 .open = proc_open_humi, 
 .read = seq_read,
 .llseek = NULL,
 .release = single_release,
};

/****************operate according to sensor chip:end************/

//function name should not be changed
static struct sensor_operate *temperature_get_ops(void)
{
	return &temperature_si7020_ops;
}


static int __init temperature_si7020_init(void)
{
	struct sensor_operate *ops = temperature_get_ops();
	int result = 0;
	int type = ops->type;
	printk("tmp_si7020 temperature_si7020_init\n");	
        proc_dir=proc_mkdir("sensors", NULL);  
        if(proc_dir==NULL)  
        {  
                return -1;  
        }  
        proc_temp = proc_create("temperature", 0444, proc_dir, &proc_temp_ops);  
        if(proc_temp == NULL)  
        {  
                remove_proc_entry("sensors", NULL);  
                return -1;  
        }
          	
        proc_humi = proc_create("humidity", 0444, proc_dir, &proc_humi_ops);  
        if(proc_humi==NULL)  
        {  
                remove_proc_entry("temperature",proc_dir);  
                remove_proc_entry("sensors", NULL);  
                return -1;  
        } 
	
	result = sensor_register_slave(type, NULL, NULL, temperature_get_ops);
	return result;
}

static void __exit temperature_si7020_exit(void)
{
	struct sensor_operate *ops = temperature_get_ops();
	int type = ops->type;
	
    remove_proc_entry("humidity", proc_dir);  
    remove_proc_entry("temperature", proc_dir);
    remove_proc_entry("sensors", NULL);  	
	
	sensor_unregister_slave(type, NULL, NULL, temperature_get_ops);
	
	
}


module_init(temperature_si7020_init);
module_exit(temperature_si7020_exit);

